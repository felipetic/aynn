<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\V1\CitiesController as CitiesV1;
use App\Http\Controllers\Api\V1\CustomersController as CustomersV1;
use App\Http\Controllers\Api\V1\OrderDetailController as OrderDetailV1;
use App\Http\Controllers\Api\V1\OrdersController as OrdersV1;
use App\Http\Controllers\Api\V1\ProductsController as ProductsV1;

// Rutas agrupadas V1
Route::prefix('v1')->group(function () {

    //con autenticacion sanctum

    Route::apiResource('cities', CitiesV1::class)
        ->only(['show', 'index', 'store', 'update', 'destroy'])
        ->middleware('auth:sanctum');

    Route::apiResource('customers', CustomersV1::class)
        ->only(['show', 'index', 'store', 'update', 'destroy'])
        ->middleware('auth:sanctum');

    Route::apiResource('orderdetail', OrderDetailV1::class)
        ->only(['show', 'index', 'store', 'update', 'destroy'])
        ->middleware('auth:sanctum');

    Route::apiResource('orders', OrdersV1::class)
        ->only(['show', 'index', 'store', 'update', 'destroy'])
        ->middleware('auth:sanctum');

    Route::apiResource('products', ProductsV1::class)
        ->only(['show', 'index', 'store', 'update', 'destroy'])
        ->middleware('auth:sanctum');
});

// Login
Route::post('login', [
    App\Http\Controllers\Api\LoginController::class,
    'login'
]);

// Rutas sin agrupar V1
// Route::apiResource('v1/cities', CitiesV1::class)
//     ->only(['show', 'index', 'store']);

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/