<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('order_id')->autoIncrement();
            $table->unsignedBigInteger('customer_id')->nullable(false);
            $table->date('order_date')->nullable(false);
            $table->decimal('order_total', 15, 2)->nullable(false);
            $table->date('order_date_delivery')->nullable(false);
            $table->char('order_status', 10)->nullable(false);
            $table->foreign('customer_id')->references('customer_id')->on('customers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
