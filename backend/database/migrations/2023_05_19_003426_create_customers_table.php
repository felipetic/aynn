<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->unsignedBigInteger('customer_id')->autoIncrement();
            $table->char('customer_id_number', 50)->nullable(false);
            $table->char('customer_name', 50)->nullable(false);
            $table->date('customer_birth_date')->nullable(false);
            $table->char('customer_address', 100)->nullable(false);
            $table->char('customer_phone', 50)->nullable(false);
            $table->unsignedBigInteger('city_id')->nullable(false);
            $table->foreign('city_id')->references('city_id_id')->on('cities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
