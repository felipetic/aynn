<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CustomersFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'customer_id_number' => rand(1, 10),
            'customer_name' => $this->faker->name,
            'customer_birth_date' => $this->faker->date,
            'customer_address' => $this->faker->address,
            'customer_phone' => $this->faker->phoneNumber,
            'city_id' => rand(1, 10)
        ];
    }
}