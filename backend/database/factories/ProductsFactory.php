<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_description' => $this->faker->text(50),
            'product_amount' => rand(1, 10),
            'product_value' => rand(1, 10),
            'product_status' => $this->faker->randomElement(['active', 'inactive'])

        ];
    }
}
