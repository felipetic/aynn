<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class OrdersFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'customer_id' => rand(1, 10),
            'order_date' => $this->faker->date,
            'order_total' => rand(1, 10),
            'order_date_delivery' => $this->faker->date,
            'order_status' => $this->faker->randomElement(['pending', 'delivered', 'canceled'])
        ];
    }
}
