<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

-   [Simple, fast routing engine](https://laravel.com/docs/routing).
-   [Powerful dependency injection container](https://laravel.com/docs/container).
-   Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
-   Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
-   Database agnostic [schema migrations](https://laravel.com/docs/migrations).
-   [Robust background job processing](https://laravel.com/docs/queues).
-   [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

-   **[Vehikl](https://vehikl.com/)**
-   **[Tighten Co.](https://tighten.co)**
-   **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
-   **[64 Robots](https://64robots.com)**
-   **[Cubet Techno Labs](https://cubettech.com)**
-   **[Cyber-Duck](https://cyber-duck.co.uk)**
-   **[Many](https://www.many.co.uk)**
-   **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
-   **[DevSquad](https://devsquad.com)**
-   **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
-   **[OP.GG](https://op.gg)**
-   **[WebReinvent](https://webreinvent.com/?utm_source=laravel&utm_medium=github&utm_campaign=patreon-sponsors)**
-   **[Lendio](https://lendio.com)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

l framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

The Larave

<br><br>

# Laravel 8 API Readme

## Prueba de API con Postman

<br>

### Autenticacion de API

<br>

1. Abre Postman y realiza una solicitud POST a la siguiente URL:

    URl de ejemplo: http://localhost:8087/aynn/backend/public/api/login

2. La respuesta es:

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "email": ["The email field is required."],
        "password": ["The password field is required."],
        "name": ["The name field is required."]
    }
}
```

3. En la pestaña "Body" de Postman, selecciona la subpestaña "form-data" e ingresa los siguientes datos de inicio de sesión los cuales debes consultar en la DB en la tabla User:

En este caso solo buscas el email porque la contraseña es password y el name es el dispositivo o sistema que consume el API:

```json
    email: strosin.kacey@example.com

    password: password

    name: iphone
```

4. En la subpestaña o en el consumo del API "Headers", ingresa el siguiente encabezado:

```json
    Accept: application/json
```

    Esto permitirá que la solicitud se reconozca como una API no como solicitud de navegador.

5. Utiliza el token como encabezado en tus solicitudes para conectarte y consumir datos.

    En la pestaña de headers de Postman o en el consumo desde el frontend, agrega el siguiente encabezado el cual es la respuesta exitosa del login del API y el cual va a usar para consumir los endpoint que traen la data:

    ```json
    Authorization: Bearer 1|LLz9lV6HXtBKW7qh0vZNfXRzYK5pibemH3YPMzWH
    ```

6. Si la respuesta es correcta, se devolverá el siguiente token, lo cual indica que has iniciado sesión correctamente como ejemplo daria una respuesta como la siguiente:

```json
{
    "token": "1|LLz9lV6HXtBKW7qh0vZNfXRzYK5pibemH3YPMzWH",
    "message": "Login success"
}
```

Esto permitirá que accedas y consumas los datos protegidos por autenticación.

<br>

7. Para crear un registro debe ser pormedio del metodo POST y se debe enviar la siguiente estructura:

URL: http://localhost:8087/aynn/backend/public/api/v1/cities

```json
{
    "city_name": "CiudadEjemplo"
}
```

<br>

8. Para actulizar un registro se haria con la siguiente URL y se le pasaria al final el parametro a actulizar:

URL : http://localhost:8087/aynn/backend/public/api/v1/cities/122

La estructura seria la siguiente:

```json
{
    "city_name": "CiudadEjemploActualizada"
}
```

9. Para borrar un registro seria con el Metodo DELETE y se le pasaria el parametro al final de la URL:

URL: http://localhost:8087/aynn/backend/public/api/v1/cities/122
