<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // Login true
        if (Auth::attempt($request->only('email', 'password'))) {

            return response()->json([
                'token' => $request->user()->createToken($request->name)->plainTextToken,
                'message' => 'Login success'
            ], 200);
        }

        // Login false
        return response()->json([
            'message' => 'Unauthorized, Login failed'
        ], 401);
    }

    public function validateLogin(Request $request)
    {
        return $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'name' => 'required' // Tipo de dispositivo que se está usando
        ]);
    }
}
