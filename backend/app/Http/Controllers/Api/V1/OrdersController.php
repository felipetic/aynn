<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Orders::all();

        if (!$orders) {
            return response()->json(['message' => 'Orders not found'], 404);
        }

        return response()->json($orders, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orders = new Orders();
        // Asigna los valores recibidos del formulario a las propiedades del modelo
        $orders->customer_id = $request->input('customer_id');
        $orders->order_date = $request->input('order_date');
        $orders->order_total = $request->input('order_total');
        $orders->order_date_delivery = $request->input('order_date_delivery');
        $orders->order_status = $request->input('order_status');
        // Guarda el nuevo registro en la base de datos
        $orders->save();
        // Retorna una respuesta con el registro guardado y el código de estado 201 (Created)
        return response()->json($orders, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orders = Orders::find($id);

        if (!$orders) {
            return response()->json(['message' => 'Orders not found'], 404);
        }

        return response()->json($orders, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $orders = Orders::find($id);

        if (!$orders) {
            return response()->json(['message' => 'Orders not found'], 404);
        }

        // Actualiza los campos de las orders
        $orders->customer_id = $request->input('customer_id');
        $orders->order_date = $request->input('order_date');
        $orders->order_total = $request->input('order_total');
        $orders->order_date_delivery = $request->input('order_date_delivery');
        $orders->order_status = $request->input('order_status');
        // Guarda los cambios en la base de datos
        $orders->save();

        return response()->json($orders, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orders = Orders::find($id);

        if (!$orders) {
            return response()->json(['message' => 'Orders not found'], 404);
        }

        // Elimina las orders de la base de datos
        $orders->delete();

        return response()->json(['message' => 'Orders deleted'], 200);
    }
}