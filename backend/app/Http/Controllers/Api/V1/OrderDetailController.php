<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\OrderDetail;
use Illuminate\Http\Request;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderDetail = OrderDetail::all();

        if (!$orderDetail) {
            return response()->json(['message' => 'Order Detail not found'], 404);
        }

        return response()->json($orderDetail, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orderDetail = new OrderDetail();
        // Asigna los valores recibidos del formulario a las propiedades del modelo
        $orderDetail->order_id = $request->input('order_id');
        $orderDetail->product_id = $request->input('product_id');
        // Guarda el nuevo registro en la base de datos
        $orderDetail->save();
        // Retorna una respuesta con el registro guardado y el código de estado 201 (Created)
        return response()->json($orderDetail, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orderDetail = OrderDetail::find($id);

        if (!$orderDetail) {
            return response()->json(['message' => 'Order Detail not found'], 404);
        }

        return response()->json($orderDetail, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $orderDetail = OrderDetail::find($id);

        if (!$orderDetail) {
            return response()->json(['message' => 'Order Detail not found'], 404);
        }

        // Actualiza los campos de order detail
        $orderDetail->order_id = $request->input('order_id');
        $orderDetail->product_id = $request->input('product_id');
        // Guarda los cambios en la base de datos
        $orderDetail->save();

        return response()->json($orderDetail, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orderDetail = OrderDetail::find($id);

        if (!$orderDetail) {
            return response()->json(['message' => 'Order Detail not found'], 404);
        }

        // Elimina las order detail de la base de datos
        $orderDetail->delete();

        return response()->json(['message' => 'Order Detail deleted'], 200);
    }
}
