<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Customers;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customers::all();

        if (!$customers) {
            return response()->json(['message' => 'Customers not found'], 404);
        }

        return response()->json($customers, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customers();
        // Asigna los valores recibidos del formulario a las propiedades del modelo
        $customer->customer_id_number = $request->input('customer_id_number');
        $customer->customer_name = $request->input('customer_name');
        $customer->customer_birth_date = $request->input('customer_birth_date');
        $customer->customer_address = $request->input('customer_address');
        $customer->customer_phone = $request->input('customer_phone');
        $customer->city_id = $request->input('city_id');
        // Guarda el nuevo registro en la base de datos
        $customer->save();
        // Retorna una respuesta con el registro guardado y el código de estado 201 (Created)
        return response()->json($customer, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customers::find($id);

        if (!$customer) {
            return response()->json(['message' => 'Customer not found'], 404);
        }

        return response()->json($customer, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customers::find($id);

        if (!$customer) {
            return response()->json(['message' => 'Customer not found'], 404);
        }

        // Actualiza los campos los customers
        $customer->customer_id_number = $request->input('customer_id_number');
        $customer->customer_name = $request->input('customer_name');
        $customer->customer_birth_date = $request->input('customer_birth_date');
        $customer->customer_address = $request->input('customer_address');
        $customer->customer_phone = $request->input('customer_phone');
        $customer->city_id = $request->input('city_id');
        // Guarda los cambios en la base de datos
        $customer->save();

        return response()->json($customer, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customers::find($id);

        if (!$customer) {
            return response()->json(['message' => 'Customer not found'], 404);
        }

        // Elimina el customer de la base de datos
        $customer->delete();

        return response()->json(['message' => 'Customer deleted'], 200);
    }
}
