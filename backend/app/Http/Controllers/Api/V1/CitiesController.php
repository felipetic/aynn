<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Cities;
use Illuminate\Http\Request;

class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = Cities::all();

        if (!$cities) {
            return response()->json(['message' => 'cities not found'], 404);
        }

        return response()->json($cities, 200);

        // return Cities::latest()->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $city = new Cities;
        // Asigna los valores recibidos del formulario a las propiedades del modelo
        $city->city_name = $request->input('city_name');
        // Guarda el nuevo registro en la base de datos
        $city->save();
        // Retorna una respuesta con el registro guardado y el código de estado 201 (Created)
        return response()->json($city, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cities  $cities
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $city = Cities::find($id);

        if (!$city) {
            return response()->json(['message' => 'City not found'], 404);
        }

        return response()->json($city, 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cities  $cities
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $city = Cities::find($id);

        if (!$city) {
            return response()->json(['message' => 'City not found'], 404);
        }

        // Actualiza los campos de la ciudad
        $city->city_name = $request->input('city_name');
        // Guarda los cambios en la base de datos
        $city->save();

        return response()->json($city, 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cities  $cities
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $city = Cities::find($id);

        if (!$city) {
            return response()->json(['message' => 'City not found'], 404);
        }

        // Elimina la ciudad de la base de datos
        $city->delete();

        return response()->json(['message' => 'City deleted'], 200);
    }
}
