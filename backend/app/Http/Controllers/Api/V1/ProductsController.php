<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Products::all();

        if (!$products) {
            return response()->json(['message' => 'Products not found'], 404);
        }

        return response()->json($products, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $products = new Products();
        // Asigna los valores recibidos del formulario a las propiedades del modelo
        $products->product_description = $request->input('product_description');
        $products->product_amount = $request->input('product_amount');
        $products->product_value = $request->input('product_value');
        $products->product_status = $request->input('product_status');
        // Guarda el nuevo registro en la base de datos
        $products->save();
        // Retorna una respuesta con el registro guardado y el código de estado 201 (Created)
        return response()->json($products, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = Products::find($id);

        if (!$products) {
            return response()->json(['message' => 'Products not found'], 404);
        }

        return response()->json($products, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $products = Products::find($id);

        if (!$products) {
            return response()->json(['message' => 'Products not found'], 404);
        }

        // Actualiza los campos de los products
        $products->product_description = $request->input('product_description');
        $products->product_amount = $request->input('product_amount');
        $products->product_value = $request->input('product_value');
        $products->product_status = $request->input('product_status');
        // Guarda los cambios en la base de datos
        $products->save();

        return response()->json($products, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $products = Products::find($id);

        if (!$products) {
            return response()->json(['message' => 'Products not found'], 404);
        }

        // Elimina los products de la base de datos
        $products->delete();

        return response()->json(['message' => 'Products deleted'], 200);
    }
}
