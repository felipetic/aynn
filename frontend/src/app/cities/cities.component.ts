import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {
  cities: any[] = []; // Variable para almacenar las ciudades

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.fetchCities();
  }

  fetchCities() {
    this.http.get('http://localhost:8087/aynn/backend/public/api/v1/cities').subscribe((response) => {
      this.cities = response as any[];
    }, error => {
      console.error(error);
    });
  }
}
